import { ConflictException, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import * as bcrypt from "bcrypt"
import * as speakeasy from 'speakeasy'
import { PrismaService } from 'src/prisma/prisma.service';
import { MailerService } from 'src/mailer/mailer.service';
import { JwtService } from '@nestjs/jwt';
import { ResetPasswordConfirmationDto, ResetPasswordDto, SignInDto, SignUpDto } from './dto/auth.dto';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {


    constructor(
        private readonly prismaService: PrismaService,
        private mailerService: MailerService,
        private JwtService: JwtService,
        private confingService: ConfigService
    ) {

    }
    async signup(signupDto: SignUpDto) {

        //Verification Email et Username deja existant

       let user = await this.prismaService.user.findUnique({
            where: { email: signupDto.email }
        })

        if(user) throw new ConflictException("Email already exist")

        let pseudo = await this.prismaService.user.findUnique({
            where: { username: signupDto.username }
        })
        if (pseudo) throw new ConflictException("username already exist")

        // hash du password 
        const hashedPassword = await bcrypt.hash(signupDto.password, 10)

        //Creation de l'utilisateur dans la base 

        const createdUser = await this.prismaService.user.create({ data: { email: signupDto.email, username: signupDto.username, password: hashedPassword } })

        //Envoi du mail de confirmation 
        await this.mailerService.sendSignUpConfirmation(signupDto.email)


        return createdUser
    }

    async signin(signinDto: SignInDto) {

        const user = await this.userVerification(signinDto.email)

        const passwordVerification = await bcrypt.compare(signinDto.password, user.password)

        if (!passwordVerification) throw new UnauthorizedException("Verify your password")

        const payload = {
            sub: user.userId,
            email: user.email,
            username: user.username
        }

        const token = await this.JwtService.signAsync(payload)

        return { username: user.username, email: user.email, id: user.userId, token: token }

    }

    async resetPassword(resetPasswordDto: ResetPasswordDto) {


        const user = await this.userVerification(resetPasswordDto.email)

        const code = speakeasy.totp({ secret: this.confingService.get("OTP_CODE"), digits: 6, step: 60 * 20 })

        //ToDo renvoyer un lien vers le front

        const url = "http://localhost:3000/auth/reset-password-confirmation"

        await this.mailerService.sendResetPasswordConfirmation(user.email, url, code)

        return { msg: "reset password has been send" }

    }


    async resetPasswordConfirmation(resetPasswordConfirmationDto: ResetPasswordConfirmationDto) {

        const user = await this.userVerification(resetPasswordConfirmationDto.email)

        const codeVerification = speakeasy.totp.verify(
            {
                secret: this.confingService.get("OTP_CODE"),
                token: resetPasswordConfirmationDto.code,
                digits: 6,
                step: 60 * 20
            })
        if (!codeVerification) throw new UnauthorizedException("Invalid/expired token")

        const newPassword = await bcrypt.hash(resetPasswordConfirmationDto.password, 10)

        await this.prismaService.user.update({ where: { email: user.email }, data: { password: newPassword } })

        return { msg: "Password has sucessfully updated" }


    }

    async deleteAccount(userId: string) {

        const user = await this.prismaService.user.findUnique({ where: { userId: userId } })
        if (!user) throw new NotFoundException("user does not exist")

        const deleteConfirmation = await this.prismaService.user.delete({ where: { userId: userId } })

        return { msg: "User succesfully deleted" }

    }

    async userVerification(userEmail: string){
        let user = await this.prismaService.user.findUnique({
            where: { email: userEmail }
        })
        if (!user) throw new ConflictException("Email does not exist")
        return user
    }

}
