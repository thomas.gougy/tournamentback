import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, IsString } from "class-validator";



export class SignInDto{
    @ApiProperty()
    @IsNotEmpty()
    @IsEmail()
    email: string

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    password: string
}

export class SignUpDto{
    @ApiProperty()
    @IsNotEmpty() 
    @IsString()
    readonly username : string

    @ApiProperty()
    @IsNotEmpty() 
    @IsEmail()
    readonly email : string

    @ApiProperty()
    @IsNotEmpty() 
    @IsString()
    readonly password : string 

}

export class ResetPasswordDto{
    @ApiProperty()
    @IsEmail()
    @IsNotEmpty()
    email: string
}

export class ResetPasswordConfirmationDto{
 
    @IsEmail()
    @IsNotEmpty()
    email: string


    @IsString()
    @IsNotEmpty()
    password: string

  
    @IsString()
    @IsNotEmpty()
    code: string
}