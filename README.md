# NestJs Boiler Plate

C'est un boiler plate pour une application nestJs avec la mise en place de prisma, Auth avec JWT. 

## Les different éléments utilisés
- [NestJs](https://docs.nestjs.com/)
- [Prisma](https://www.prisma.io/docs/getting-started)
- SMTP [MailHog](https://github.com/mailhog/MailHog)

## Get Started local
 installation des package
> $ npm i

mise en place des variables d'environnement 
 
mise en place de la base avec prisma 

>$ npx prisma migrate dev




